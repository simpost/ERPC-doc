/*
 * MIT License
 * 
 * Copyright (c) 2019 极简美 @ konishi5202@163.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __RPC_CORE_H__
#define __RPC_CORE_H__
#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    ERPC_LOOP_EXIT,
    ERPC_LOOP_DEFAULT,
    ERPC_LOOP_ONCE,
    ERPC_LOOP_NOWAIT
}erpc_loop_t;

/**********************************************************
 * description: init erpc framework
 * parameter : process, current process name;
 * return : 0 , init ok;
 *         -1 , init failed;
***********************************************************/
extern int erpc_framework_init(char *process);

/**********************************************************
 * description: erpc framework main loop
 * parameter : way, you can use:
 *            ERPC_LOOP_EXIT , exit erpc framework;
 *            ERPC_LOOP_DEFAULT , monitor config-file and exception;
 *            ERPC_LOOP_ONCE , not support now;
 *            ERPC_LOOP_NOWAIT , not support now;
 * return : this function will not return until error occur;
***********************************************************/
extern int erpc_framework_loop(erpc_loop_t way);

/**********************************************************
 * description: break erpc framework loop.
***********************************************************/
extern void erpc_framework_break(void);

#ifdef __cplusplus
}
#endif

#endif  // __RPC_CORE_H__


